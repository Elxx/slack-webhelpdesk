# Requirements
* Python 2.7+

# Configure Slack
1. in Slack, click your team name at the top left and click "Team settings"
2. on the left, click "Configure apps"
3. go to the "Custom integrations" tab
4. Click "Slash commands"
5. If you've never created one before, the creation screen will come up. If you have, click "Add Configuration"
6. Pick a command and click add
7. this app will run as a web server, so set "URL" to its publicly available address or ip, e.g. "http://my-awesome-app.heroku.com/"
8. Make a copy of config.default.py and rename it to config.py
9. Edit config.py and change the SLACK_COMMAND and SLACK_TOKEN fields to match the Command and Token that you see in the Slack settings
10. Also add an API key for the help desk
11. Customize the icon, autocomplete, all that stuff, then click Save Integration

# Install Dependencies
    pip install -r requirements.txt

# Usage
    python app.py

default port is 5000, change it with the PORT env variable. or deploy it to heroku
