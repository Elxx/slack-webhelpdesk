#!/usr/bin/env python
# encoding: utf-8
import os
import sys
import json
import HTMLParser

import requests
from bottle import run, request, get, post, abort

import config

TICKET_URL = "helpdesk/WebObjects/Helpdesk.woa/ra/Tickets/{ticketNumber}/?username={username}&apiKey={apiKey}"
NOTE_URL = "helpdesk/WebObjects/Helpdesk.woa/ra/TechNotes/?username={username}&apiKey={apiKey}"

def remove_html_markup(s):
    tag = False
    quote = False
    out = ""

    for c in s:
        if c == '<' and not quote:
            tag = True
        elif c == '>' and not quote:
            tag = False
        elif (c == '"' or c == "'") and tag:
            quote = not quote
        elif not tag:
            out = out + c

    return HTMLParser.HTMLParser().unescape(out)


def br2nl(s):
    return s.replace('<br/>', '\n')


@post('/')
def handler():
    token = request.POST.get('token')
    if not token or token != config.SLACK_TOKEN:
        return abort(403, 'invalid token')

    command = request.POST.get('command')
    if not command or command != config.COMMAND:
        return abort(403, 'invalid command')

    params = request.POST.get('text')
    if not params:
        return {
            'text': 'missing ticket number'
        }

    paramParts = params.split()

    num = paramParts[0]

    if num[0] == '#':
        num = num[1:]
    if not num.isdigit():
        return {
            'text': 'invalid ticket number'
        }
    if not num:
        return {
            'text': 'no ticket number specified'
        }

    validActions = ["u", "c"]
    action = None
    validOptions = []
    options = []
    message = None
    if len(paramParts) > 1:
        if len(paramParts[1]) >= 0 and paramParts[1][0] == "-":
            for a in validActions:
                if a in paramParts[1]:
                    # only allow one action per command
                    if action:
                        return {
                            'text': 'cannot perform more than one action at a time'
                        }
                    action = a
            if not action:
                validOptions = ["p", "v"]
            elif action == "u":
                validOptions = ["p", "h", "c", "r"]

            for o in validOptions:
                if o in paramParts[1]:
                    options.append(o)

        if action or len(options):
            message = " ".join(paramParts[2:])
        else:
            message = " ".join(paramParts[1:])

    slackUsername = request.POST.get('user_name')

    r = requests.get(config.HELPDESK_URL + TICKET_URL.format(ticketNumber=num, username=slackUsername, apiKey=config.API_KEY))

    if r.status_code != 200:
        return {
            'text': 'Error occurred when trying to fetch this ticket. Status code: ' + str(r.status_code)
        }

    try:
        rData = r.json()
    except:
        return {
            'text': 'Error occurred when trying to parse ticket data',
            'attachments': [
                {
                    'text': sys.exc_info()[0]
                }
            ]
        }

    reporterUsername = ""
    if rData['clientReporter'].has_key('username'):
        reporterUsername = rData['clientReporter']['username']

    isAdmin = slackUsername in config.ADMIN_USERS
    isAuthor = slackUsername == reporterUsername

    if not action:
        if not isAdmin and not isAuthor:
            return {
                'text': 'Access denied.'
            }

        isVerbose = "v" in options
        isPublic = "p" in options

        return respond_info(rData, isPublic, isVerbose)
    elif action == "u":
        if not isAdmin:
            return {
                'text': 'Access denied.'
            }

        if not message:
            return {
                'text': 'Please include a message.'
            }

        isPublic = "p" in options
        isHidden = "h" in options

        statusId = None
        if "r" in options:
            statusId = config.RESPONSE_REQUESTED_STATUS_ID
        elif "c" in options:
            statusId = config.CLOSED_STATUS_ID

        return update_ticket(slackUsername, num, rData, message, statusId, isPublic, isHidden)


def respond_info(rData, isPublic, isVerbose):
    responseType = 'in_channel' if isPublic else 'ephemeral'

    if isVerbose:
        return build_response(rData, responseType)

    return build_brief(rData, responseType)


def update_ticket(username, ticketNumber, rData, message, statusId, isPublic, isHidden):
    payload = {
        "noteText": message,
        "jobticket":{
            "type": "JobTicket",
            "id": int(ticketNumber)
        },
        "isHidden": isHidden,
        "isSolution": statusId == config.CLOSED_STATUS_ID,
        "statusTypeId": statusId,
        #"workTime": "0",
        #"emailClient": not isSilent,
        #"emailTech": False,
        #"emailTechGroupLevel": False,
        #"emailGroupManager": False,
        #"emailCc": False,
        #"emailBcc": False,
        #"ccAddressesForTech": "",
        #"bccAddresses": ""
    }

    r = requests.post(
        config.HELPDESK_URL + NOTE_URL.format(username=username, apiKey=config.API_KEY),
        data=json.dumps(payload)
    )

    if r.status_code != 201:
        return {
            'text': 'Error occurred when trying to update this ticket. Status code: ' + str(r.status_code)
        }

    try:
        tData = r.json()
    except:
        return {
            'text': 'Error occurred when trying to parse update data',
            'attachments': [
                {
                    'text': sys.exc_info()[0]
                }
            ]
        }

    responseType = 'in_channel' if isPublic else 'ephemeral'
    title = "Note Added"
    if statusId == config.RESPONSE_REQUESTED_STATUS_ID:
        title = "Response Requested"
    elif statusId == config.CLOSED_STATUS_ID:
        title = "Ticket Closed"
    return {
        'response_type': responseType,
        'attachments': [
            get_summary(rData),
            {
                'title': title,
                'text': remove_html_markup(tData['noteText'])
            }
        ]
    }


def build_response(rData, responseType):
    response = {
        'response_type': responseType,
        'attachments': [
            {
                'title': rData['subject'],
                'title_link': rData['bookmarkableLink'],
                'fields': [
                    {
                        'title': 'Reporter',
                        'value': rData['clientReporter']['firstName'] + ' ' + rData['clientReporter']['lastName']
                    },
                    {
                        'title': 'Assignee',
                        'value': (rData['clientTech']['displayName'] if rData['clientTech'] else 'Unassigned')
                    },
                    {
                        'title': 'Status',
                        'value': rData['statustype']['statusTypeName']
                    }
                ]
            },
            {
                'title': 'Request Detail',
                'color': config.REQUEST_COLOR,
                'text': remove_html_markup(rData['detail'])
            }
        ]
    }

    if len(rData['notes']):
        latestNote = rData['notes'][:1][0]

        if latestNote['type'] == 'TechNote':
            color = config.TECH_NOTE_COLOR
        else:
            color = config.USER_COLOR

        response['attachments'].append({
            'title': 'Latest Status',
            'color': color,
            'text': remove_html_markup(br2nl(latestNote['prettyUpdatedString'])) + ':\r\r' + remove_html_markup(br2nl(latestNote['mobileNoteText']))
        })

    return response


def get_summary(rData):
    return {
        'title': rData['subject'],
        'title_link': rData['bookmarkableLink'],
        'text': ' | '.join(
            [
                '*Reporter:* ' + rData['clientReporter']['firstName'] + ' ' + rData['clientReporter']['lastName'],
                '*Assignee:* ' + (rData['clientTech']['displayName'] if rData['clientTech'] else 'Unassigned'),
                '*Status:* ' + rData['statustype']['statusTypeName']
            ]
        ),
        'mrkdwn_in': ['text']
    }


def build_brief(rData, responseType):
    response = {
        'response_type': responseType,
        'attachments': [
            get_summary(rData)
        ]
    }

    if len(rData['notes']):
        latestNote = rData['notes'][:1][0]

        if latestNote['type'] == 'TechNote':
            color = config.TECH_NOTE_COLOR
        else:
            color = config.USER_COLOR

        response['attachments'].append({
            'color': color,
            'text': remove_html_markup(br2nl(latestNote['prettyUpdatedString'])) + ':\r\r' + remove_html_markup(br2nl(latestNote['mobileNoteText']))
        })

    return response


if __name__ == '__main__':
    port = os.environ.get('PORT', 5000)
    run(host='0.0.0.0', port=port)
